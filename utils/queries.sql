{% sql 'get_creative_text' %}
select text
from creative
where creativeid = %(creativeid)s
{% endsql %}


{% sql 'get_transaction_segments' %}
select
    segment
    , value
from
    tblsegmentpresignup tsp
join
    tblsegmentkey tsk
    on tsk.segmentkeyid = tsp.segmentkeyid
join
    tblsegmentvalue tsv
    on tsv.segmentvalueid = tsp.segmentvalueid
where
    memberid = %(memberid)s
{% endsql %}


{% sql 'get_user_segments' %}
    select
        suburb
        , state
        , postcode
        , gender
        , age
        , birthdate
        , subscribed
        , firstnameenc
        , lastnameenc
    from tblmember
    where
        memberid = %(memberid)s
{% endsql %}
