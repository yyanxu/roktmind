import {Component, EventEmitter, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {DataService} from '../data.service';

@Component({
  selector: 'app-creative-dialog',
  templateUrl: './creative-dialog.component.html',
  styleUrls: ['./creative-dialog.component.scss']
})
export class CreativeDialogComponent {

  title: string;
  result: any;
  raw: any;

  public event: EventEmitter<any> = new EventEmitter();

  constructor(
    public dialogRef: MatDialogRef<CreativeDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public dlg_data: any,
    public data: DataService
  ) {
    this.title = "Creative Comparison"
    this.result = dlg_data.element.result;
    this.raw = dlg_data.element.raw_creative;
    console.log('received data is', dlg_data, 'result is', this.result);
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onSubmit(): void {
  }

}
