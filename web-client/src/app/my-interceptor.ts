import { Injectable, Inject } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { delay } from 'rxjs/operators';
import { BackendResponse } from './backend-response';
import { DataService } from './data.service';

@Injectable({
  providedIn: 'root'
})
export class MyInterceptor implements HttpInterceptor {

  /*   _index = 0;
    get index(): number {
      const result = this._index;
      this._index = (this._index + 1) % this.dataSource.length;
      return result;
    }
   */
  constructor(protected data: DataService, @Inject('shouldIntercept') protected shouldIntercept) { }
  /* 
    dataSource = [
      { creativeId: 'Post One', memberId: 'Web Development', result: 'Body 1' },
      { creativeId: 'Post Two', memberId: 'Android Development', result: 'Body 2' },
      { creativeId: 'Post Three', memberId: 'IOS Development', result: 'Body 3' },
      { creativeId: 'Post Four', memberId: 'Android Development', result: 'Body 4' },
      { creativeId: 'Post Five', memberId: 'IOS Development', result: 'Body 5' },
      { creativeId: 'Post Six', memberId: 'Web Development', result: 'Body 6' },
    ];
   */
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<BackendResponse>> {
    let data, params;
    if (!this.shouldIntercept) {
      return next.handle(req);
    }

    if (req.url === this.data.url) {
      params = req.params;
      // console.log(`url ${req.urlWithParams}`);
      data = {
        url: req.urlWithParams,
        creativeId: params.get('creativeId'),
        memberId: params.get('memberId'),
        funny: params.get('funny'),
        modelId: params.get('modelId'),
        createdAt: new Date(),
        result: 'some funny text goes here',
        raw_creative: 'raw'
      };
      // console.log('returning fake response', data);
      return of(new HttpResponse(<BackendResponse>{ body: data })).pipe(
        delay(2000)
      );
    } else if (req.url === this.data.creativeIdsUrl) {
      // console.log(`url ${req.urlWithParams}`);
      return of(new HttpResponse({
        body: [
          { creativeId: 1 },
          { creativeId: 2 },
          { creativeId: 3 },
          { creativeId: 4 },
          { creativeId: 5 },
          { creativeId: 6 },
        ]
      })).pipe(
        delay(100)
      );
    } else if (req.url === this.data.memberIdsUrl) {
      // console.log(`url ${req.urlWithParams}`);
      return of(new HttpResponse({
        body: [
          { memberId: 1 },
          { memberId: 2 },
          { memberId: 3 },
          { memberId: 4 },
          { memberId: 5 },
          { memberId: 6 },
        ]
      })).pipe(
        delay(100)
      );
    } else if (req.url === this.data.modelIdsUrl) {
      // console.log(`url ${req.urlWithParams}`);
      return of(new HttpResponse({
        body: [
          { modelId: 'Markov' },
          { modelId: 'RNN' },
        ]
      })).pipe(
        delay(100)
      );
    } else {
      return next.handle(req);
    }
  }
}
