import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, BehaviorSubject, of } from 'rxjs';
import { delay, finalize } from 'rxjs/operators';
import { Params } from '@angular/router';
import { BackendResponse } from './backend-response';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  get url() {
    return 'http://localhost:5000/roktmind/optimal_creative';
  }

  get creativeIdsUrl() {
    return 'http://localhost:5000/creativeIds';
  }

  get memberIdsUrl() {
    return 'http://localhost:5000/memberIds';
  }

  get modelIdsUrl() {
    return 'http://localhost:5000/modelIds';
  }

  isBusy$: Observable<boolean>;

  _isBusy$ = new BehaviorSubject<boolean>(false);

  set isBusy(value) {
    of(null).pipe(
      delay(0),
    ).subscribe(() => {
      this._isBusy$.next(value)
    });
  }

  constructor(protected http: HttpClient) {
    this.isBusy$ = this._isBusy$.asObservable();
   }

  getCreatives() {
    this.isBusy = true;
    return this.http.get<any>(this.creativeIdsUrl).pipe(
      finalize(() => this.isBusy = false)
    );
  }

  getMembers() {
    this.isBusy = true;
    return this.http.get<any>(this.memberIdsUrl).pipe(
      finalize(() => this.isBusy = false)
    );
  }

  getModels() {
    this.isBusy = true;
    return this.http.get<any>(this.modelIdsUrl).pipe(
      finalize(() => this.isBusy = false)
    );
  }

  getCreative(queryParams: Params) {
    this.isBusy = true;
    return this.http.get<BackendResponse>(this.url, { params: queryParams }).pipe(
      finalize(() => this.isBusy = false)
    );
  }
}
