import { Component } from '@angular/core';
import { DataService } from './data.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  color = 'accent'; //'#2199F4';  'primary', 'accent'
  mode = 'indeterminate';
  value = '';
  bufferValue = '';

  constructor(public data: DataService) {}
}
