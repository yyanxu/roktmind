import { Component, OnInit, ViewChild } from '@angular/core';
import { Params } from '@angular/router';
import { MatSelect } from '@angular/material';
import { MatDialog } from '@angular/material';
import { Observable, BehaviorSubject } from 'rxjs';
import { filter } from 'rxjs/operators';
import { DataService } from '../data.service';
import { CreativeDialogComponent } from '../creative-dialog/creative-dialog.component';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styles: []
})
export class DashboardComponent implements OnInit {

  creatives$: Observable<any[]>;
  members$: Observable<any[]>;
  models$: Observable<any[]>;

  results$: Observable<any[]>;

  results = [];
  _results: BehaviorSubject<any[]>;

  @ViewChild('memberId', { read: MatSelect }) memberIdComp;
  @ViewChild('creativeId', { read: MatSelect }) creativeIdComp;
  @ViewChild('modelId', { read: MatSelect }) modelIdComp;

  displayedColumns = ['createdAt', 'creativeId', 'memberId', 'modelId', 'funny', 'result'];

  constructor(protected data: DataService,
     protected dialog: MatDialog) {
    this._results = new BehaviorSubject(this.results);
    this.results$ = this._results.pipe(
      filter((r) => r.length > 0)
    );
  }

  ngOnInit() {
    this.creatives$ = this.data.getCreatives();
    this.members$ = this.data.getMembers();
    this.models$ = this.data.getModels();
  }

  getResult(haveFun) {
    const queryParams: Params = {
      creativeId: this.creativeIdComp.value,
      memberId: this.memberIdComp.value,
      modelId: this.modelIdComp.value,
      funny: haveFun
    };
    this.data.getCreative(queryParams).subscribe(r => {
      // console.log('received ', r);
      this.results = [...this.results, r];
      this._results.next(this.results);
    });
  }

  openDialog(element) {
    console.log('open dialog with', element);
    let dialogRef = this.dialog.open(CreativeDialogComponent, {
      width: '600px',
      data: {
        title: 'Creative',
        element: element,
      }
    });
    dialogRef.componentInstance.event.subscribe((result) => {
    });
  }
}
