import os

import scrapy

DIR = os.path.abspath(os.path.dirname(__file__))

def normalize(s):
    return ' '.join((s or '').split())


class HotelReviewSpider(scrapy.Spider):
    name = "rokathon"

    _number = 0

    @property
    def number(self):
        self._number += 1
        return self._number

    @property
    def next_filename(self):
        filename = os.path.join(DIR, os.path.join('../crawled_data', 'data-%s.html' % self.number))
        return filename

    def start_requests(self):
        # need to crawl all a.hotel_name_link.url
        urls = [
            # 'https://www.booking.com/searchresults.en-gb.html?aid=304142&label=gen173nr-1FCAEoggJCAlhYSDNYBGgPiAEBmAEuuAEHyAEM2AEB6AEB-AELkgIBeagCAw&sid=ed7d6d737d6a7e07052d5f5bf8357391&sb=1&src=country&src_elem=sb&error_url=https%3A%2F%2Fwww.booking.com%2Fcountry%2Fnz.en-gb.html%3Faid%3D304142%3Blabel%3Dgen173nr-1FCAEoggJCAlhYSDNYBGgPiAEBmAEuuAEHyAEM2AEB6AEB-AELkgIBeagCAw%3Bsid%3Ded7d6d737d6a7e07052d5f5bf8357391%3Binac%3D0%26%3B&ss=Christ+Church%2C+Caribbean+Islands%2C+Barbados&checkin_monthday=21&checkin_month=6&checkin_year=2018&checkout_monthday=22&checkout_month=6&checkout_year=2018&no_rooms=1&group_adults=2&group_children=0&from_sf=1&ss_raw=Christ+Church&ac_position=0&ac_langcode=en&dest_id=900040308&dest_type=city&place_id_lat=13.0519&place_id_lon=-59.510601&search_pageview_id=e38e02393ea90095&search_selected=true&search_pageview_id=e38e02393ea90095&ac_suggestion_list_length=5&ac_suggestion_theme_list_length=0'
            # 'https://www.booking.com/searchresults.en-gb.html?label=gen173nr-1FCAQoggJCDmNpdHlfOTAwMDQwMzA4SAlYBGgPiAEBmAEuuAEHyAEP2AEB6AEB-AECkgIBeagCAw&sid=0cc4278e558c388f2cc0732e67ce2886&sb=1&src=theme_landing&src_elem=sb&error_url=https%3A%2F%2Fwww.booking.com&nflt=sth%253D1&region=2804&checkin_year=&checkin_month=&checkout_year=&checkout_month=&no_rooms=1&group_adults=2&group_children=0&from_sf=1',
            # 'https://www.booking.com/searchresults.en-gb.html?label=gen173nr-1DCAsoE0IWemVuYnJlYWstbWFsb25leS1kcml2ZUgJWARoD4gBAZgBLrgBB8gBDNgBA-gBAfgBApICAXmoAgM&sid=ed7d6d737d6a7e07052d5f5bf8357391&sb=1&src=country&src_elem=sb&error_url=https%3A%2F%2Fwww.booking.com%2Fcountry%2Fus.en-gb.html%3Flabel%3Dgen173nr-1DCAsoE0IWemVuYnJlYWstbWFsb25leS1kcml2ZUgJWARoD4gBAZgBLrgBB8gBDNgBA-gBAfgBApICAXmoAgM%3Bsid%3Ded7d6d737d6a7e07052d5f5bf8357391%3B&ss=New+York%2C+New+York+State%2C+USA&checkin_monthday=21&checkin_month=6&checkin_year=2018&checkout_monthday=22&checkout_month=6&checkout_year=2018&no_rooms=1&group_adults=2&group_children=0&from_sf=1&ss_raw=New+Y&ac_position=0&ac_langcode=en&dest_id=20088325&dest_type=city&place_id_lat=40.768074&place_id_lon=-73.981895&search_pageview_id=dd8f1812b3f60096&search_selected=true',
            # 'https://www.booking.com/searchresults.en-gb.html?aid=304142&label=gen173nr-1FCAEoggJCAlhYSDNYBGgPiAEBmAEuuAEHyAEM2AEB6AEB-AELkgIBeagCAw&sid=ed7d6d737d6a7e07052d5f5bf8357391&checkin=2018-06-21&checkout=2018-06-22&city=-1575736&from=900040308&postcard=1&sr_click_postcard=1',
            # 'https://www.booking.com/searchresults.en-gb.html?aid=304142&label=gen173nr-1FCAEoggJCAlhYSDNYBGgPiAEBmAEuuAEHyAEM2AEB6AEB-AELkgIBeagCAw&sid=ed7d6d737d6a7e07052d5f5bf8357391&sb=1&src=searchresults&src_elem=sb&error_url=https%3A%2F%2Fwww.booking.com%2Fsearchresults.en-gb.html%3Faid%3D304142%3Blabel%3Dgen173nr-1FCAEoggJCAlhYSDNYBGgPiAEBmAEuuAEHyAEM2AEB6AEB-AELkgIBeagCAw%3Bsid%3Ded7d6d737d6a7e07052d5f5bf8357391%3Bcheckin_month%3D6%3Bcheckin_monthday%3D21%3Bcheckin_year%3D2018%3Bcheckout_month%3D6%3Bcheckout_monthday%3D22%3Bcheckout_year%3D2018%3Bcity%3D-1575736%3Bclass_interval%3D1%3Bdest_id%3D-1456928%3Bdest_type%3Dcity%3Bdtdisc%3D0%3Bfrom_sf%3D1%3Bgroup_adults%3D2%3Bgroup_children%3D0%3Binac%3D0%3Bindex_postcard%3D0%3Blabel_click%3Dundef%3Bno_rooms%3D1%3Boffset%3D0%3Bpostcard%3D0%3Braw_dest_type%3Dcity%3Broom1%3DA%252CA%3Bsb_price_type%3Dtotal%3Bsearch_selected%3D1%3Bsrc%3Dsearchresults%3Bsrc_elem%3Dsb%3Bss%3DParis%252C%2520Ile%2520de%2520France%252C%2520France%3Bss_all%3D0%3Bss_raw%3DParis%3Bssb%3Dempty%3Bsshis%3D0%3Bssne_untouched%3DGold%2520Coast%26%3B&ss=Los+Angeles%2C+California%2C+USA&ssne=Paris&ssne_untouched=Paris&city=-1456928&checkin_monthday=21&checkin_month=6&checkin_year=2018&checkout_monthday=22&checkout_month=6&checkout_year=2018&group_adults=2&group_children=0&no_rooms=1&from_sf=1&ss_raw=Los+Angele&ac_position=0&ac_langcode=en&dest_id=20014181&dest_type=city&place_id_lat=34.052051&place_id_lon=-118.243269&search_pageview_id=f3561999f67d0174&search_selected=true&search_pageview_id=f3561999f67d0174&ac_suggestion_list_length=5&ac_suggestion_theme_list_length=0',
            # 'https://www.booking.com/searchresults.en-gb.html?aid=304142&label=gen173nr-1FCAEoggJCAlhYSDNYBGgPiAEBmAEuuAEHyAEM2AEB6AEB-AELkgIBeagCAw&sid=ed7d6d737d6a7e07052d5f5bf8357391&sb=1&src=searchresults&src_elem=sb&error_url=https%3A%2F%2Fwww.booking.com%2Fsearchresults.en-gb.html%3Faid%3D304142%3Blabel%3Dgen173nr-1FCAEoggJCAlhYSDNYBGgPiAEBmAEuuAEHyAEM2AEB6AEB-AELkgIBeagCAw%3Bsid%3Ded7d6d737d6a7e07052d5f5bf8357391%3Bcheckin_month%3D6%3Bcheckin_monthday%3D21%3Bcheckin_year%3D2018%3Bcheckout_month%3D6%3Bcheckout_monthday%3D22%3Bcheckout_year%3D2018%3Bcity%3D-1456928%3Bclass_interval%3D1%3Bdest_id%3D20014181%3Bdest_type%3Dcity%3Bdtdisc%3D0%3Bfrom_sf%3D1%3Bgroup_adults%3D2%3Bgroup_children%3D0%3Binac%3D0%3Bindex_postcard%3D0%3Blabel_click%3Dundef%3Bno_rooms%3D1%3Boffset%3D0%3Bpostcard%3D0%3Braw_dest_type%3Dcity%3Broom1%3DA%252CA%3Bsb_price_type%3Dtotal%3Bsearch_selected%3D1%3Bsrc%3Dsearchresults%3Bsrc_elem%3Dsb%3Bss%3DLos%2520Angeles%252C%2520California%252C%2520USA%3Bss_all%3D0%3Bss_raw%3DLos%2520Angele%3Bssb%3Dempty%3Bsshis%3D0%3Bssne_untouched%3DParis%26%3B&ss=Chicago&ssne=Los+Angeles&ssne_untouched=Los+Angeles&city=20014181&checkin_monthday=21&checkin_month=6&checkin_year=2018&checkout_monthday=22&checkout_month=6&checkout_year=2018&group_adults=2&group_children=0&no_rooms=1&from_sf=1',
            # 'https://www.booking.com/searchresults.en-gb.html?aid=304142&label=gen173nr-1FCAEoggJCAlhYSDNYBGgPiAEBmAEuuAEHyAEM2AEB6AEB-AELkgIBeagCAw&sid=ed7d6d737d6a7e07052d5f5bf8357391&sb=1&src=searchresults&src_elem=sb&error_url=https%3A%2F%2Fwww.booking.com%2Fsearchresults.en-gb.html%3Faid%3D304142%3Blabel%3Dgen173nr-1FCAEoggJCAlhYSDNYBGgPiAEBmAEuuAEHyAEM2AEB6AEB-AELkgIBeagCAw%3Bsid%3Ded7d6d737d6a7e07052d5f5bf8357391%3Bcheckin_month%3D6%3Bcheckin_monthday%3D21%3Bcheckin_year%3D2018%3Bcheckout_month%3D6%3Bcheckout_monthday%3D22%3Bcheckout_year%3D2018%3Bcity%3D20014181%3Bclass_interval%3D1%3Bdest_id%3D20033173%3Bdest_type%3Dcity%3Bdtdisc%3D0%3Bfrom_sf%3D1%3Bgroup_adults%3D2%3Bgroup_children%3D0%3Binac%3D0%3Bindex_postcard%3D0%3Blabel_click%3Dundef%3Bno_rooms%3D1%3Boffset%3D0%3Bpostcard%3D0%3Broom1%3DA%252CA%3Bsb_price_type%3Dtotal%3Bsrc%3Dsearchresults%3Bsrc_elem%3Dsb%3Bss%3DChicago%3Bss_all%3D0%3Bssb%3Dempty%3Bsshis%3D0%3Bssne_untouched%3DLos%2520Angeles%26%3B&ss=Las+Vegas%2C+Nevada%2C+USA&ssne=Chicago&ssne_untouched=Chicago&city=20033173&checkin_monthday=21&checkin_month=6&checkin_year=2018&checkout_monthday=22&checkout_month=6&checkout_year=2018&group_adults=2&group_children=0&no_rooms=1&from_sf=1&ss_raw=Las+Vegas&ac_position=0&ac_langcode=en&dest_id=20079110&dest_type=city&place_id_lat=36.118999&place_id_lon=-115.167999&search_pageview_id=452e19be28c4009c&search_selected=true'            
            'https://www.booking.com/searchresults.en-gb.html?aid=304142&label=gen173nr-1FCAEoggJCAlhYSDNYBGgPiAEBmAEuuAEHyAEM2AEB6AEB-AELkgIBeagCAw&sid=ed7d6d737d6a7e07052d5f5bf8357391&sb=1&src=searchresults&src_elem=sb&error_url=https%3A%2F%2Fwww.booking.com%2Fsearchresults.en-gb.html%3Faid%3D304142%3Blabel%3Dgen173nr-1FCAEoggJCAlhYSDNYBGgPiAEBmAEuuAEHyAEM2AEB6AEB-AELkgIBeagCAw%3Bsid%3Ded7d6d737d6a7e07052d5f5bf8357391%3Bcheckin_month%3D6%3Bcheckin_monthday%3D21%3Bcheckin_year%3D2018%3Bcheckout_month%3D6%3Bcheckout_monthday%3D22%3Bcheckout_year%3D2018%3Bcity%3D20033173%3Bclass_interval%3D1%3Bdest_id%3D20079110%3Bdest_type%3Dcity%3Bdtdisc%3D0%3Bfrom_sf%3D1%3Bgroup_adults%3D2%3Bgroup_children%3D0%3Binac%3D0%3Bindex_postcard%3D0%3Blabel_click%3Dundef%3Bno_rooms%3D1%3Boffset%3D0%3Bpostcard%3D0%3Braw_dest_type%3Dcity%3Broom1%3DA%252CA%3Bsb_price_type%3Dtotal%3Bsearch_selected%3D1%3Bsrc%3Dsearchresults%3Bsrc_elem%3Dsb%3Bss%3DLas%2520Vegas%252C%2520Nevada%252C%2520USA%3Bss_all%3D0%3Bss_raw%3DLas%2520Vegas%3Bssb%3Dempty%3Bsshis%3D0%3Bssne_untouched%3DChicago%26%3B&ss=Houston%2C+Texas%2C+USA&ssne=Las+Vegas&ssne_untouched=Las+Vegas&city=20079110&checkin_monthday=21&checkin_month=6&checkin_year=2018&checkout_monthday=22&checkout_month=6&checkout_year=2018&group_adults=2&group_children=0&no_rooms=1&from_sf=1&ss_raw=Houston&ac_position=0&ac_langcode=en&dest_id=20128761&dest_type=city&place_id_lat=29.76&place_id_lon=-95.362503&search_pageview_id=4dda1a094bd902cb&search_selected=true&search_pageview_id=4dda1a094bd902cb&ac_suggestion_list_length=5&ac_suggestion_theme_list_length=0',
            # 'https://www.booking.com/searchresults.en-gb.html?aid=304142&label=gen173nr-1FCAEoggJCAlhYSDNYBGgPiAEBmAEuuAEHyAEM2AEB6AEB-AELkgIBeagCAw&sid=ed7d6d737d6a7e07052d5f5bf8357391&sb=1&src=searchresults&src_elem=sb&error_url=https%3A%2F%2Fwww.booking.com%2Fsearchresults.en-gb.html%3Faid%3D304142%3Blabel%3Dgen173nr-1FCAEoggJCAlhYSDNYBGgPiAEBmAEuuAEHyAEM2AEB6AEB-AELkgIBeagCAw%3Bsid%3Ded7d6d737d6a7e07052d5f5bf8357391%3Bcheckin_month%3D6%3Bcheckin_monthday%3D21%3Bcheckin_year%3D2018%3Bcheckout_month%3D6%3Bcheckout_monthday%3D22%3Bcheckout_year%3D2018%3Bcity%3D20079110%3Bclass_interval%3D1%3Bdest_id%3D20128761%3Bdest_type%3Dcity%3Bdtdisc%3D0%3Bfrom_sf%3D1%3Bgroup_adults%3D2%3Bgroup_children%3D0%3Binac%3D0%3Bindex_postcard%3D0%3Blabel_click%3Dundef%3Bno_rooms%3D1%3Boffset%3D0%3Bpostcard%3D0%3Braw_dest_type%3Dcity%3Broom1%3DA%252CA%3Bsb_price_type%3Dtotal%3Bsearch_selected%3D1%3Bsrc%3Dsearchresults%3Bsrc_elem%3Dsb%3Bss%3DHouston%252C%2520Texas%252C%2520USA%3Bss_all%3D0%3Bss_raw%3DHouston%3Bssb%3Dempty%3Bsshis%3D0%3Bssne_untouched%3DLas%2520Vegas%26%3B&ss=Orlando&ssne=Houston&ssne_untouched=Houston&city=20128761&checkin_monthday=21&checkin_month=6&checkin_year=2018&checkout_monthday=22&checkout_month=6&checkout_year=2018&group_adults=2&group_children=0&no_rooms=1&from_sf=1'

        ]
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        page = response.url
        hotel_links = response.css('a.hotel_name_link::attr(href)')
        for link in hotel_links:
            href = normalize(link.extract())
            hotel_page = response.urljoin(href)
            self.log('Searching hotel page %s' % hotel_page)
            yield scrapy.Request(hotel_page, callback=self.parse_hotel_page)
        next_page = response.css('a.paging-next::attr(href)').extract_first()
        if next_page.startswith('/'):
            next_page = response.urljoin(next_page)
        if next_page:
            yield scrapy.Request(next_page, callback=self.parse)

    def parse_hotel_page(self, response):
        url = response.url
        hotel_name = normalize(response.css('#hp_hotel_name::text').extract_first())
        summary = response.css('#summary *::text')
        summary_data = normalize(' '.join([x.extract() for x in summary]))
        address = normalize(response.css('.hp_address_subtitle::text').extract_first())
        comments = response.css('.hp-social_proof .hp-social_proof-item .hp-social_proof-quote_bubble::text')
        comments_data = [i.strip(u'\u201c\u201d') for i in map(normalize, [c.extract() for c in comments]) if i]
        yield (dict(
            hotel=hotel_name,
            url=url,
            address=address,
            summary=summary_data,
            comments=comments_data,
        ))
