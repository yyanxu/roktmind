import datetime

from flask import Flask, redirect

from flask import jsonify, request
from flask import send_file

from generator.creative_generator import generate_personalized_creative

app = Flask(__name__)


@app.route("/")
def whoami():
    return send_file('index.html')


@app.route('/creativeIds')
def get_creative_ids():
 #    creativeIds = '''
 #    2425661627577929454
 # 2425661627577929454
 # 2425661627577929454
 # 2425661627577929454
 # 2425661627577929454
 # 2427932333837681566
 # 2425661627577929454
 # 2425661627577929454
 # 2425661627577929454
 # 2425661627577929454'''.split()
    creativeIds = ['2233408604961785376']
    return jsonify([dict(creativeId=i) for i in creativeIds])


@app.route('/memberIds')
def get_member_ids():
    memberIds = '''
    6831627822
 6836620155
 6831629777
 6831630084
 6831630579
 6836622786
 6836624328
 6831633037
 6836624681
 6831633933 '''.split()
    # memberIds = ['6861694854']
    return jsonify([dict(memberId=i) for i in memberIds])


@app.route('/modelIds')
def get_model_ids():
    modelIds = ['markov', 'rnn']
    return jsonify([dict(modelId=i) for i in modelIds])


@app.route('/roktmind/optimal_creative/')
def generate_creative():
    creativeid = request.args['creativeId']
    memberid = request.args['memberId']
    funny = request.args['funny'] == 'true'
    model = request.args['modelId']

    print("{} {}".format(funny, model))

    d = generate_personalized_creative(creativeid, memberid, funny=funny, model=model)

    result = dict(
        createdAt=datetime.datetime.now().strftime('%Y-%m-%dT%H:%M:%SZ'),
        creativeId=creativeid,
        memberId=memberid,
        modelId=model,
        funny=funny,
    )
    result.update(d)
    result['result'] = result['solutions']
    return jsonify(**result)


# @app.route('/modelIds')
# def get_model_ids():
#     return redirect("http://www.example.com")


@app.route('/roktmind/optimal_creative_v2/<int:creativeid>/<int:memberid>')
def generate_creative_v2(creativeid, memberid):
    # creativeid = request.args['creativeId']
    # memberid = request.args['memberId']
    # funny = request.args['funny'] == 'true'
    # model = request.args['modelId']

    return jsonify(generate_personalized_creative(creativeid, memberid))


if __name__ == '__main__':
    app.secret_key = 'ROCKMind'
    app.run(debug=True)
