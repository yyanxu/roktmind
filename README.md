# flask-app


## Setup Development Environment

### Prerequisites

Please make sure following prerequisites are all installed.

 - python3.6

### Install virtualenv

Please follow the instructions in https://virtualenv.pypa.io/en/stable/installation/ to install virtualenv first.

### Clone code repo and install dependencies

```

$ virtualenv venv

$ . venv/bin/activate

$ pip install -r requirements.txt
```

### run the code

PYTHONPATH="${PYTHONPATH}:~/Documents/workspace/roktmind" python api/web_service.py


### Markov Generator
$ cd generator/model_training/markov
$ python markov_generator.py houston

### RNN Generator
With RNN, the seed text can be a sentence. 
$ cd generator/model_training/rnn
$ python generator.py ## you can change the seed_text in the script. 
