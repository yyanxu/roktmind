import re
import html

from keyword_extract.extractor_method import KeywordExtract
from utils.data_loader import get_creative_text, get_user_segments, get_publisher_segments

def get_user_keywords(conn, memberid):
    text = get_user_segments(conn, memberid)
    #text = html.unescape(clean_html(text))
    print(text)

    return text

def get_advertiser_keywords(conn, creative_id):
    text = get_creative_text(conn, creative_id)
    text = html.unescape(clean_html(text))
    print(text)

    text_service = KeywordExtract()
    text_service.extract_keywords_from_text(text)
    return text_service.get_ranked_phrases(), text

def get_publisher_keywords(conn, memberid):
    text = get_publisher_segments(conn, memberid)
    text = {k.lower(): v for k, v in text.items()}
    #text = html.unescape(clean_html(text))
    print(text)
    return text

def clean_html(raw_html):
    cleanr = re.compile('<.*?>')
    cleantext = re.sub(cleanr, '', raw_html)
    return cleantext
