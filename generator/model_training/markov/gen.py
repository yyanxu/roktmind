from generator.model_training.markov.parse import Parser

class Generator:
  def __init__(self, name, db, rnd):
    self.name = name
    self.db   = db
    self.rnd  = rnd

  def _get_next_word(self, word_list):
    candidate_words = self.db.get_word_count(word_list)
    total_next_words = sum(candidate_words.values())
    # import pdb; pdb.set_trace()
    i = self.rnd.randint(total_next_words)
    t=0
    for w in candidate_words.keys():
      t += candidate_words[w]
      if (i <= t):
        return w
    assert False

  def generate(self, word_separator, start_words=['newyork']): #['travel']):
    depth = self.db.get_depth()
    sentence = start_words #[Parser.SENTENCE_START_SYMBOL] * (depth - 1) #(1+len(start_words)))  + start_words
    # sentence = ['view']
    if len(self.db.get_word_count(start_words)) == 0:
      # sentence = ['newyork']
      return 'Cannot find any result!'
    end_symbol = [Parser.SENTENCE_END_SYMBOL] * (depth - 1)
    idx = 0
    while True:
      tail = sentence[(-depth+1):]
      # tail = 'city'
      if tail == end_symbol:
        break
      # if idx == 0:
      #   tail[-1] = ['travel']
      word = self._get_next_word(tail)
      sentence.append(word)
    
    return word_separator.join(sentence[:][:1-depth])
