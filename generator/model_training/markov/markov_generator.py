from generator.model_training.markov.gen import Generator
from generator.model_training.markov.parse import Parser
from generator.model_training.markov.sql import Sql
from generator.model_training.markov.rnd import Rnd
import sys
import sqlite3
import codecs
import os

from generator.model_training.markov.db import Db


DB_PATH = os.path.dirname(__file__) + '/booking.db'


def generate_something_markov(seed):
  WORD_SEPARATOR = ' '
  db = Db(sqlite3.connect(DB_PATH), Sql())
  generator = Generator('booking', db, Rnd())
  return generator.generate(WORD_SEPARATOR, start_words=[seed])


if __name__ == '__main__':
  WORD_SEPARATOR = ' '
  word = sys.argv[1]
  db = Db(sqlite3.connect(DB_PATH), Sql())
  generator = Generator('booking', db, Rnd())
  print(generator.generate(WORD_SEPARATOR, start_words=[word]))