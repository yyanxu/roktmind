import string
import os
import re
import argparse
import codecs


def clean_doc(doc):
  ''' Tokenize the doc
  '''
  doc = doc.replace('--', ' ')
  tokens = doc.split()
  # remove punctuation from each token
  # "#$%&'()*+,-/:<=>@[\]^_`{|}~
  puncts = re.sub(r'[!;.?]+', '', string.punctuation) #string.punctuation.replace('!', '').replace(';', '').replace('.', '').replace('?', '')
  table = str.maketrans('', '', puncts)
  tokens = [w.translate(table) for w in tokens]
  tokens = [word for word in tokens if word.isalpha()]
  tokens = [word.lower() for word in tokens]
  return tokens

# save tokens to file, one dialog per line
def save_doc(lines, filename):
  data = '\n'.join(lines)
  file = open(filename, 'w')
  file.write(data)
  file.close()

def main():
  # Parse args
  parser = argparse.ArgumentParser()
  parser.add_argument("-i", default = "corpus/hotel_review.txt")
  args = parser.parse_args()

  # with  codecs.open(args.i , 'r', 'ISO-8859-1') as inf:
  with  codecs.open(args.i , 'r', 'utf-8') as inf:
    prefix = os.path.splitext(args.i)[0] + '_'
    print(prefix)
    doc = inf.read()
    tokens = clean_doc(doc)
    print(tokens[:200])
    print('Total Tokens: %d' % len(tokens))
    print('Unique Tokens: %d' % len(set(tokens)))

    # organize into sequences of tokens
    length = 20 + 1
    sequences = list()
    for i in range(length, len(tokens)):
      # select sequence of tokens
      seq = tokens[i-length:i]
      line = ' '.join(seq)
      sequences.append(line)
    print('Total Sequences: %d' % len(sequences))

    # save sequences to file
    out_filename = prefix+'corpus.txt'
    save_doc(sequences, out_filename)

if __name__ == '__main__':
  main()