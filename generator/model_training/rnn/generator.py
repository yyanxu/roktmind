from random import randint
from pickle import load
import argparse
from keras.models import load_model
from keras.preprocessing.sequence import pad_sequences

# generate a sequence from a language model
def generate_seq(model, tokenizer, seq_length, seed_text, n_words):
  result = list()
  in_text = seed_text
  # generate a fixed number of words
  for _ in range(n_words):
    # encode the text as integer
    encoded = tokenizer.texts_to_sequences([in_text])[0]
    # truncate sequences to a fixed length
    encoded = pad_sequences([encoded], maxlen=seq_length, truncating='pre')
    # predict probabilities for each word
    yhat = model.predict_classes(encoded, verbose=0)
    # map predicted word index to word
    out_word = ''
    for word, index in tokenizer.word_index.items():
      if index == yhat:
        out_word = word
        break
    # append to input
    in_text += ' ' + out_word
    result.append(out_word)
  return ' '.join(result)

def main():
  parser = argparse.ArgumentParser()
  parser.add_argument("-p", default = "hotel_review_")
  parser.add_argument("-m", default = "2lstm_100it_")
  args = parser.parse_args()

  prefix = args.p #'hotel_review_'
  method = args.m #'2lstm_100it_'
  corpus = 'corpus/' + prefix + 'corpus.txt'
  with open(corpus, 'r') as inf:
    doc = inf.read()
    lines = doc.split('\n')

    seq_length = len(lines[0].split()) - 1
    print(seq_length)
    
    # load the model
    model = load_model('models/'+prefix+method+'model.h5')

    # load the tokenizer
    tokenizer = load(open('models/'+prefix+method+'tokenizer.pkl', 'rb'))

    # select a seed text
    # seed_text = lines[randint(0,len(lines))]
    #seed_text = 'great choice for travellers' # 
    #seed_text = 'great location' # 'lovely town'
    seed_text = 'beyonce hotel location'
    seed_text = 'new york' # top hotel offer'
    # seed_text = 'spring holidays resolution with'
    seed_text = 'Need to book a hotel for your trip'
    print(seed_text + '\n')

    # generate new text
    generated = generate_seq(model, tokenizer, seq_length, seed_text, 15)
    print(generated)

if __name__ == '__main__':
  main()