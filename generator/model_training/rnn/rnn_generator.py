from random import randint
from pickle import load
import argparse
from keras.models import load_model
from keras.preprocessing.sequence import pad_sequences
import os

MODEL_PATH = os.path.dirname(__file__) + '/models/booking_summaries_lstm_50itmodel.h5'
TOK_PATH = os.path.dirname(__file__) + '/models/booking_summaries_lstm_50ittokenizer.pkl'

# generate a sequence from a language model
def generate_seq(model, tokenizer, seq_length, seed_text, n_words):
  result = list()
  in_text = seed_text
  # generate a fixed number of words
  for _ in range(n_words):
    # encode the text as integer
    encoded = tokenizer.texts_to_sequences([in_text])[0]
    # truncate sequences to a fixed length
    encoded = pad_sequences([encoded], maxlen=seq_length, truncating='pre')
    # predict probabilities for each word
    yhat = model.predict_classes(encoded, verbose=0)
    # map predicted word index to word
    out_word = ''
    for word, index in tokenizer.word_index.items():
      if index == yhat:
        out_word = word
        break
    # append to input
    in_text += ' ' + out_word
    result.append(out_word)
  return ' '.join(result)

def generate_something_rnn(seed_text):
  seq_length = 20
  # load the model
  model = load_model(MODEL_PATH)

  # load the tokenizer
  tokenizer = load(open(TOK_PATH, 'rb'))

  # select a seed text
  #seed_text = 'great choice for travellers' # 
  #seed_text = 'great location' # 'lovely town'
  # seed_text = 'beyonce hotel location'
  # seed_text = 'new york' # top hotel offer'
  # seed_text = 'spring holidays resolution with'
  # seed_text = 'Need to book a hotel for your trip'
  # print(seed_text + '\n')
  
  # generate new text
  generated = generate_seq(model, tokenizer, seq_length, seed_text, 15)
  return generated

if __name__ == '__main__':
  generate_something_rnn("Need to book a hotel for your trip")
