from numpy import array
from pickle import dump
import argparse
from keras.preprocessing.text import Tokenizer
from keras.utils import to_categorical
from keras.models import Sequential
from keras.layers import Dense, LSTM, Embedding, Dropout


def main():
  parser = argparse.ArgumentParser()
  parser.add_argument("-p", default = "hotel_review_")
  parser.add_argument("-m", default = "2lstm_100it_")
  args = parser.parse_args()

  prefix = args.p #'hotel_review_'
  method = args.m #'2lstm_100it_'
  corpus = 'corpus/' + prefix + 'corpus.txt'
  with open(corpus, 'r') as inf:
    doc = inf.read()
    lines = doc.split('\n')
    # lines = inf.readlines()

    # integer encode sequences of words
    tokenizer = Tokenizer()
    tokenizer.fit_on_texts(lines)
    sequences = tokenizer.texts_to_sequences(lines)
    # vocabulary size
    vocab_size = len(tokenizer.word_index) + 1

    # separate into input and output
    sequences = array(sequences)
    X, y = sequences[:,:-1], sequences[:,-1]
    y = to_categorical(y, num_classes=vocab_size)
    seq_length = X.shape[1]

    # define model
    model = Sequential()
    model.add(Embedding(vocab_size, 10, input_length=seq_length))
    model.add(LSTM(100, return_sequences=True))
    # model.add(Dropout(0.2))
    model.add(LSTM(100))
    # model.add(Dropout(0.2))
    model.add(Dense(100, activation='relu'))
    model.add(Dense(vocab_size, activation='softmax'))
    print(model.summary())
    
    # compile model
    model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
    # fit model
    model.fit(X, y, batch_size=128, epochs=50)

    # save the model to file
    model.save('models/'+prefix+method+'model.h5')
    # save the tokenizer
    dump(tokenizer, open('models/'+prefix+method+'tokenizer.pkl', 'wb'))

if __name__ == '__main__':
  main()