import pdb

from generator.model_training.markov.markov_generator import generate_something_markov
from generator.model_training.rnn.rnn_generator import generate_something_rnn
from keyword_extract.extractor_helper import get_user_keywords, get_advertiser_keywords, get_publisher_keywords
import pytest
from string import Template

from utils.data_loader import get_db_connection

MAX_KEYWORD_LEN = 10


def generate_personalized_creative(creative_id, memberid, funny=False, model="rnn"):
    db_client = get_db_connection()
    db_client.connect()

    customer_keywords = get_user_keywords(db_client.get_conn(), memberid)
    creative_keywords, raw_creative = get_advertiser_keywords(db_client.get_conn(), creative_id)
    publisher_keywords = get_publisher_keywords(db_client.get_conn(), memberid)

    solution = Template(
        "${username}, as a thank you for purchasing the ${eventname}, here is a $$50 off coupon from Hotels.com for your trip to ${eventcity}. ${markov} Signup to the e-newsletter now to get your coupon code. T&Cs apply.")

    # print(publisher_keywords['eventcity'])

    # import pytest
    # pytest.set_trace()

    something = ""
    if funny:
        if model == "markov":
            something = generate_something_markov(publisher_keywords['eventcity'].lower()) + "."
        else:
            # something = generate_something_rnn("New York USA") + "."
            keywords = raw_creative + publisher_keywords['eventcity'].lower()
            something = generate_something_rnn(keywords) + "."
    publisher_keywords['markov'] = something

    return {
        "customer_keywords": customer_keywords,
        "creative_keywords": creative_keywords,
        "publisher_keywords": publisher_keywords,
        "raw_creative": raw_creative,
        "solutions": solution.substitute(publisher_keywords)
    }
